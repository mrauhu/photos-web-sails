/**
 * FileController
 *
 * @description :: Server-side logic for managing files
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  /**
   * For testing upload
   * @param req
   * @param res
   */
  index: function( req, res ) {
    Student.find()
      .exec( function( err, students) {
        if (err) {
          return res.serverError('Failed get students', err);
        }
        return res.view({
          students: students
        });
      });
  },

  /**
   * Upload photos
   * @param req
   * @param res
   */
  upload: function( req, res ) {
    if ( typeof req.param('id') === 'undefined') {
      return res.badRequest('Upload photos without student id');
    }
    var student = req.param('id');

    req.file('file').upload(function (err, files) {
      if (err) {
        return res.serverError(err);
      }

      // Add user field
      for ( var n in files ) {
        if ( files.hasOwnProperty( n ) ) {
          files[ n ].student = student;
        }
      }
      console.info( files );

      File.create( files )
        .exec( function ( err, files ) {
          if ( err ) {
            return res.serverError('Upload files problem', err);
          }
          return res.redirect('/student/photos/' + student );
        });
    });
  }
};


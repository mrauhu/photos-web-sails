/**
 * StudentController
 *
 * @description :: Server-side logic for managing students
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  /**
   * Add student
   * @param req
   * @param res
   */
  add: function(req, res) {
    if (typeof req.body === 'undefined'
      || typeof req.body.name === 'undefined' ) {
      return res.view({});
    }
    Student.create({
      name: req.body.name
    }).exec( function( err, student ) {
      if (err) {
        return res.serverError('Error creating student', err);
      }
      return res.redirect('/');
    });
  },

  /**
   * List of students
   * route: /
   * @param req
   * @param res
   */
	list: function(req, res) {
    Student.find()
      .exec( function( err, students ){
        if ( err ) {
          return serverError('Error get student', err);
        }
        return res.view({
          students: students
        });
      });
  },

  /**
   * List of photos of student
   * route: /student/photos/:id
   * @param req
   * @param res
   */
  photos: function( req, res ) {
    if ( typeof req.param('id') === 'undefined') {
      return res.badRequest('Photos without student id');
    }
    Student.findOne({
      id: req.param('id')
    }).populate('files')
      .exec( function( err, student ){
        if (err) {
          return serverError('Error get student', err);
        }
        console.info('Student files', student, req.param('id') );
      return res.view({
        student: student
      });
    })
  }
};


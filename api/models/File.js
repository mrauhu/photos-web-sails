/**
* File.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    student: {
      model: 'student',
      required: true
    },
    path: {
      type: 'string'
    },
    name: {
      type: 'string'
    }

  },

  /**
   * Add name & path fields to file
   * @param file
   * @param next
   */
  beforeCreate: function (file, next) {
    var path = require('path');
    file.name = path.basename( file.fd );
    // TODO: Add rule to Nginx, for: /uploads -> /.tmp/uploads
    file.path = '/uploads/' + file.name;

    console.log('beforeCreate file', file );

    next( null, file);
  }

};


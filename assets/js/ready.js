$(document).ready( function(){
  /**
   * After .confirm make ajax call to a[href]
   */
  $('.confirm').click( function( event ){
    event.preventDefault();
    var action = window.confirm('Удалить?'),
      $link = $(this);

    if ( action ) {
      $.ajax( {
        url: $link[0].href,
        method: 'get'
      }).done( function( response ){
        // Remove student row
        $( '#student-' + $link.data('student') )
          .hide('slow');
      });
    }
  });
});
